package messages;

import java.io.Serializable;

public class ChatReqMsg implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5628432267438366357L;
	private final String message;
	private final CriticalStatus criticalSection;

	public ChatReqMsg(String message, CriticalStatus criticalSection) {
		super();
		this.message = message;
		this.criticalSection = criticalSection;
	}

	public String getMessage() {
		return message;
	}
	
	public CriticalStatus getCriticalSection() {
		return criticalSection;
	}

}
