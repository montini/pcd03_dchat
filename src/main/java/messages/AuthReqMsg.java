package messages;

import java.io.Serializable;

public class AuthReqMsg implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -514730186467058832L;
	private final int ID;
	private final int priority;
	private final int msgHash;
	public AuthReqMsg(int iD, int priority, int msgHash) {
		super();
		ID = iD;
		this.priority = priority;
		this.msgHash = msgHash;
	}
	public int getID() {
		return ID;
	}
	public int getPriority() {
		return priority;
	}
	public int getMsgHash() {
		return msgHash;
	}
}
