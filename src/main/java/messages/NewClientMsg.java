package messages;

import java.io.Serializable;

import akka.actor.ActorRef;

public class NewClientMsg implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 522820625775521122L;
	private final ActorRef ref;
	private final String name;

	public NewClientMsg(final ActorRef ref, final String name) {
		super();
		this.ref = ref;
		this.name = name;
	}

	public ActorRef getRef() {
		return ref;
	}	
	
	public String getName() {
		return name;
	}	

}
