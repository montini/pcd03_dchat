package messages;

import java.io.Serializable;
import java.util.Map;

import akka.actor.ActorRef;

public class ParticipantsMsg implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4304594306282784478L;
	private final Map<ActorRef, String> participants;

	public ParticipantsMsg(Map<ActorRef, String> participants) {
		this.participants = participants;
	}

	public Map<ActorRef, String> getParticipants() {
		return participants;
	}
	
}
