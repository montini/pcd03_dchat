package messages;

import java.io.Serializable;

public class AuthResponseMsg implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8111729362799456756L;
	private final boolean auth;
	private final int msgHash;
	public AuthResponseMsg(boolean auth, int msgHash) {
		super();
		this.auth = auth;
		this.msgHash = msgHash;
	}
	public boolean isAuth() {
		return auth;
	}
	public int getMsgHash() {
		return msgHash;
	}
}
