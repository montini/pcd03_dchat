package messages;

import java.io.Serializable;

public class ChatMsg implements Serializable {
	
	private static final long serialVersionUID = 9135831374931422757L;
	private final String senderName;
	private final String message;
	private final CriticalStatus criticalSection;

	public ChatMsg(String senderName, String message, CriticalStatus criticalSection) {
		super();
		this.senderName = senderName;
		this.message = message;
		this.criticalSection = criticalSection;
	}
	
	public String getSenderName() {
		return senderName;
	}
	
	public String getMessage() {
		return message;
	}
	
	public CriticalStatus getCriticalSection() {
		return criticalSection;
	}
}
