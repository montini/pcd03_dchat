package messages;

import java.io.Serializable;

public enum CriticalStatus implements Serializable {
	JOIN,
	NEUTRAL,
	LEAVE;

}
