package actors;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Optional;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import akka.actor.AbstractActorWithStash;
import akka.actor.ActorRef;
import akka.actor.ActorSelection;
import akka.actor.ReceiveTimeout;
import akka.actor.Terminated;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import main.MainClient;
import messages.AuthReqMsg;
import messages.AuthResponseMsg;
import messages.NewClientMsg;
import messages.ParticipantsMsg;
import scala.concurrent.duration.Duration;
import messages.ChatMsg;
import messages.ChatReqMsg;
import messages.CriticalStatus;
import view.ChatLine;
import view.ClientGUI;

public class ClientActor extends AbstractActorWithStash {

	private ActorSelection registry;

	private String name;

	private int priority;
	private Queue<ChatMsg> queue;
	private int authReceived;

	private Optional<ActorRef> criticalClient;

	public static ObservableList<ChatLine> chatLog =  FXCollections.observableArrayList();
	public static ObservableList<String> participantsList =  FXCollections.observableArrayList();
	private Map<ActorRef, String> clients = new HashMap<>();

	@Override
	public void preStart() throws Exception {		
		this.name = "Client " + MainClient.clientID;

		this.registry = getContext().actorSelection("akka.tcp://MySystem@127.0.0.1:2552/user/registry");
		this.registry.tell(new NewClientMsg(getSelf(), this.name), getSelf());

		this.priority = 0;
		this.authReceived = 0;
		this.queue = new LinkedList<>();
		this.criticalClient = Optional.empty();
		ClientGUI.setActor(this);
	}	

	@Override
	public Receive createReceive() {
		return receiveBuilder().match(ParticipantsMsg.class, msg -> {
			// Just joined the chat
			this.clients.clear();
			this.clients.putAll(msg.getParticipants());

			this.clients.keySet().forEach(c -> getContext().watch(c));

			Platform.runLater(() -> {
				participantsList.clear();
				participantsList.addAll(clients.values());
			});
		}).match(NewClientMsg.class, msg -> {
			// A new client joined
			this.clients.put(msg.getRef(), msg.getName());

			getContext().watch(msg.getRef());

			Platform.runLater(() -> {
				participantsList.add(msg.getName());
				chatLog.add(new ChatLine("System", msg.getName() + " has joined the chat."));
			});

		}).match(ChatReqMsg.class, msg -> {
			// Someone requested to send a message
			ChatMsg message = new ChatMsg(this.name, msg.getMessage(), msg.getCriticalSection());
			this.queue.add(message);
			MainClient.log("GUI cerca di inviare, in coda " + this.queue.size());
			if (this.queue.size() == 1 && csCheck()) {
				this.smartSendMsg(message);
			}

		}).match(AuthReqMsg.class, msg -> {
			// I'm receiving requests to send a message from the sender
			boolean authorized;
			MainClient.log(getSender() + " asks for auth");
			if (getSender().equals(getSelf()) || this.queue.isEmpty() || msg.getPriority() > this.priority 
					|| (this.criticalClient.isPresent() && this.criticalClient.get().equals(getSender()))) {
				authorized = true;
				MainClient.log("REQ AUTH per Sender == Self || coda vuota || priorit� || � in cs");
			} else {
				if (msg.getID() > this.name.hashCode()) {
					authorized = true;
					MainClient.log("REQ AUTH per id maggiore");
				} else {
					authorized = false;
					MainClient.log("REQ AUTH negato");
				}
			}

			AuthResponseMsg authRespMsg;
			if (authorized) {
				authRespMsg = new AuthResponseMsg(true, msg.getMsgHash());
			} else {
				authRespMsg = new AuthResponseMsg(false, msg.getMsgHash());
			}

			getSender().tell(authRespMsg, getSelf());

		}).match(AuthResponseMsg.class, msg -> {
			// I'm receiving authorizations to send my enqueued message
			if (this.queue.isEmpty() || msg.getMsgHash() != this.queue.peek().getMessage().hashCode()) {
				MainClient.log("Wrong auth, lo salto (in coda " + this.queue.size() + ")");
			} else {
				if (msg.isAuth()) {
					this.authReceived++;
					MainClient.log("Auth (" + this.authReceived + ") ricevuto da " + getSender());
					if (this.authReceived == this.clients.size()) {
						MainClient.log("Auth ricevuti, invio il messaggio");
						ChatMsg message = this.queue.poll();
						this.clients.keySet().forEach(c -> c.tell(message, getSelf()));
						this.authReceived = 0;
						this.priority = 0;
					}
				} else {
					MainClient.log("Not authorized");
					this.priority++;
				}
			}
		}).match(ChatMsg.class, msg -> {
			// Message received			
			if (msg.getCriticalSection().equals(CriticalStatus.JOIN)) {
				this.setupCriticalJoin(msg);
			} else {
				if (msg.getCriticalSection().equals(CriticalStatus.LEAVE)) {
					this.setupCriticalLeave(msg);
				} else {
					this.appendChatLine(msg);
				}
			}

			if (!this.queue.isEmpty() && this.csCheck()) {
				MainClient.log("Ricevuto e coda non vuota, ritento il mio");
				this.smartSendMsg(this.queue.peek());
			}

		}).match(ReceiveTimeout.class, m -> {
			// Critical Section time expired
			//TODO
			getContext().setReceiveTimeout(Duration.Undefined());
			MainClient.log("CS Timeout");
			this.clients.keySet().forEach(c -> c.tell(new ChatMsg(this.name, "", CriticalStatus.LEAVE), getSelf()));

		}).match(Terminated.class, msg -> {
			ActorRef dead = msg.getActor();
			String name = this.clients.get(dead);
			Platform.runLater(() -> {
				participantsList.remove(name);
				chatLog.add(new ChatLine("System", name + " has left the chat."));
			});
			this.clients.remove(dead);

			if (this.criticalClient.isPresent() && this.criticalClient.get().equals(dead)) {
				getSelf().tell(new ChatMsg("", "", CriticalStatus.LEAVE), dead);
			}

		}).build();
	}

	public void sendMsgRequest(String msg) {
		getSelf().tell(new ChatReqMsg(msg, CriticalStatus.NEUTRAL), getSelf());
	}

	public void sendCriticalSectionInfo(CriticalStatus status) {
		getSelf().tell(new ChatReqMsg("", status), getSelf());
	}

	private void smartSendMsg(ChatMsg message) {
		this.authReceived = 0;
		MainClient.log("Richiedo gli auth");

		if (MainClient.DEBUG_MODE) {
			this.clients.keySet().forEach(c -> {
				new Thread(() -> {
					try {Thread.sleep(new Random().nextInt(2345));} catch (InterruptedException e1) {e1.printStackTrace();}
					c.tell(new AuthReqMsg(this.name.hashCode(), this.priority, message.getMessage().hashCode()), getSelf());
				}).start();	

			});
		} else {
			this.clients.keySet().forEach(c -> {
				c.tell(new AuthReqMsg(this.name.hashCode(), this.priority, message.getMessage().hashCode()), getSelf());
			});
		}
	}

	private void appendChatLine(ChatMsg msg) {
		final CountDownLatch doneLatch = new CountDownLatch(1);
		Platform.runLater(() -> {
			try {
				chatLog.add(new ChatLine(msg.getSenderName(),msg.getMessage()));
			} finally {doneLatch.countDown();}
		});
		try {doneLatch.await();} catch (InterruptedException e) {}
	}

	//true se posso non ci sono altri in cs
	private boolean csCheck() {
		return !this.criticalClient.isPresent() || this.criticalClient.get().equals(getSelf());
	}

	private void setupCriticalJoin(ChatMsg msg) {
		if (getSender().equals(getSelf())) {
			getContext().setReceiveTimeout(Duration.create(MainClient.CS_DURATION_SECONDS, TimeUnit.SECONDS));
		}
		this.appendChatLine(new ChatMsg("System", msg.getSenderName() + " is now in the critical section", msg.getCriticalSection()));
		this.criticalClient = Optional.of(getSender());
		if (!csCheck()) {
			ClientGUI.csBusy(true);
		}
	}

	private void setupCriticalLeave(ChatMsg msg) {
		this.appendChatLine(new ChatMsg("System", msg.getSenderName() + " is no longer in the critical section", msg.getCriticalSection()));
		this.criticalClient = Optional.empty();
		ClientGUI.csBusy(false);
	}

}