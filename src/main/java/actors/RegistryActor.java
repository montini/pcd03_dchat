package actors;

import java.util.HashMap;
import java.util.Map;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Terminated;
import messages.NewClientMsg;
import messages.ParticipantsMsg;

public class RegistryActor extends AbstractActor {

	private Map<ActorRef, String> clients;

	@Override
	public void preStart() {
		this.clients = new HashMap<>();
	}	

	@Override
	public Receive createReceive() {
		return receiveBuilder().match(NewClientMsg.class, msg -> {
			ActorRef newClient = msg.getRef();

			this.clients.put(newClient, msg.getName());				
			this.clients.keySet().stream().filter(p -> !p.equals(newClient)).forEach(p -> p.tell(new NewClientMsg(msg.getRef(), msg.getName()), getSelf()));
			getContext().watch(newClient);
			
			newClient.tell(new ParticipantsMsg(this.clients), getSelf());
 
		}).match(Terminated.class, msg -> {
			this.clients.remove(msg.getActor());
		}).build();
	}
}