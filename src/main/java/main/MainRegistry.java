package main;

import java.io.File;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import actors.RegistryActor;
import akka.actor.ActorSystem;
import akka.actor.Props;

public class MainRegistry {

	public static void main(String[] args) {
		try {
			Config config = ConfigFactory.parseFile(new File("registry.conf"));
			ActorSystem system = ActorSystem.create("MySystem",config);
			system.actorOf(Props.create(RegistryActor.class),"registry");
		} catch (Exception e) {
			System.err.println("Registry is already running");
			System.exit(1);
		}
		
	}

}
