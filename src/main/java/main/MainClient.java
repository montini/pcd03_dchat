package main;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Random;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import actors.ClientActor;
import akka.actor.ActorSystem;
import akka.actor.Props;
import javafx.application.Application;
import view.ClientGUI;

public class MainClient {

	public static int clientID;
	public static boolean DEBUG_MODE = false;
	public static boolean VERBOSE = true;
	public static int CS_DURATION_SECONDS = 5;

	public static void main(String[] args) throws IOException {

		clientID = new Random().nextInt(500);
		System.out.println(clientID);
		int port = 4500 + clientID;

		String configString = String.join("\n",Files.readAllLines(new File("client.conf").toPath()));
		configString = configString.replaceAll("%port%", port+"");

		Config config = ConfigFactory.parseString(configString);

		try {
			ActorSystem system = ActorSystem.create("MySystem",config);
			system.actorOf(Props.create(ClientActor.class),"client" + clientID);
			Application.launch(ClientGUI.class);
		} catch (Exception e) {
			System.out.println("Looking for a new ID");
			main(args);
		}		
	}
	
	public static void log(String msg) {
		if (MainClient.VERBOSE) {
			System.out.println(msg);	
		}
		
	}

}
