package view;

import actors.ClientActor;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.Separator;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import main.MainClient;
import messages.CriticalStatus;

public class ClientGUI extends javafx.application.Application implements EventHandler<ActionEvent>  {

	private static ClientActor actor;

	private static Button sendButton;
	private static Button enterCritButton;
	private static Button leaveCritButton;
	private static TextArea messageArea;

	private static double WIDTH = 500.0;
	private static double HEIGHT = 650.0;

	@Override
	public void start(Stage primaryStage) throws Exception {
		primaryStage.setTitle("Chat Client - client" + MainClient.clientID);
		VBox mainBox = new VBox(20);
		mainBox.setAlignment(Pos.CENTER);

		HBox centerBox = new HBox(20);
		centerBox.setPrefWidth(600);

		ListView<ChatLine> chatLog;		 
		chatLog = new ListView<>();
		chatLog.setItems(ClientActor.chatLog);
		chatLog.setCellFactory(cell -> new ChatLineCell());
		chatLog.setPrefWidth(400);

		ListView<String> participants;
		participants = new ListView<>();
		participants.setItems(ClientActor.participantsList);

		centerBox.getChildren().addAll(chatLog, participants);

		HBox bottomBox = new HBox(20);
		bottomBox.setPadding(new Insets(15, 12, 15, 12));
		bottomBox.setMinHeight(150);
		bottomBox.setAlignment(Pos.CENTER);

		messageArea = new TextArea();
		messageArea.setOnKeyPressed(e -> {
			if (e.getCode().equals(KeyCode.ENTER)) {
				sendButton.fire();
			}
		});
		//messageArea.maxWidth(WIDTH-100);

		sendButton = new Button("Send");
		sendButton.setOnAction(this);
		sendButton.setMinWidth(100);
		
		enterCritButton = new Button("Enter CS");
		enterCritButton.setOnAction(this);
		enterCritButton.setMinWidth(100);
		
		leaveCritButton = new Button("Leave CS");
		leaveCritButton.setOnAction(this);
		leaveCritButton.setMinWidth(100);
		leaveCritButton.setDisable(true);
		
		VBox buttons = new VBox(20);
		Separator hSep = new Separator();
		hSep.setOrientation(Orientation.HORIZONTAL);
		buttons.setAlignment(Pos.CENTER);
		buttons.getChildren().addAll(sendButton, hSep, enterCritButton, leaveCritButton);

		bottomBox.getChildren().add(messageArea);
		bottomBox.getChildren().add(buttons);


		mainBox.getChildren().addAll(centerBox,bottomBox);
		Scene scene = new Scene(mainBox, WIDTH, HEIGHT);
		primaryStage.setScene(scene);
		primaryStage.show();

	}

	private static int count;
	@Override
	public void handle(ActionEvent ev) {
		new Thread(() -> {
			Object src = ev.getSource();
			if (src.equals(sendButton)) {
				if (MainClient.DEBUG_MODE) {
					actor.sendMsgRequest(count++ +"");
				} else {
					if (messageArea.getText().length() > 0) {
						actor.sendMsgRequest(messageArea.getText());
						messageArea.clear();	
					}
				}
			} else if (src.equals(enterCritButton)) {
				actor.sendCriticalSectionInfo(CriticalStatus.JOIN);
				
				leaveCritButton.setDisable(false);
				enterCritButton.setDisable(true);
			} else if (src.equals(leaveCritButton)) {
				actor.sendCriticalSectionInfo(CriticalStatus.LEAVE);
				
				leaveCritButton.setDisable(true);
				enterCritButton.setDisable(false);
			}
		}).start();
	}

	static public void setActor(ClientActor actor) throws Exception {
		if (ClientGUI.actor == null) {
			ClientGUI.actor = actor;
		} else {
			throw new Exception();
		}
	}
	
	static public void csBusy(boolean isBusy) {
		if (isBusy) {
			leaveCritButton.setDisable(true);
			enterCritButton.setDisable(true);
		} else {
			leaveCritButton.setDisable(true);
			enterCritButton.setDisable(false);
		}
	}

	@Override
	public void stop() {
		Platform.exit();
		System.exit(0);
	}


}
