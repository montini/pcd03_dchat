package view;

import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;

public class ChatLineCell extends ListCell<ChatLine> {

	@Override
	protected void updateItem(ChatLine chatLine, boolean empty) {
		super.updateItem(chatLine, empty);

		if(empty || chatLine == null) {

			setText(null);
			setGraphic(null);

		} else {
			
			Label senderLbl = new Label();
			senderLbl.setText(chatLine.getSenderName() + ": ");
			senderLbl.setFont(javafx.scene.text.Font.font(null, FontWeight.BOLD, 12));	
			
			int hash = chatLine.getSenderName().hashCode();
			int r = (hash & 0xFF0000) >> 16;
			int g = (hash & 0x00FF00) >> 8;
			int b = hash & 0x0000FF;
			
			senderLbl.setTextFill(Color.rgb(r, g, b));
			
			Label messageLbl = new Label();
			messageLbl.setText(chatLine.getMessage());
			if (chatLine.getSenderName().equals("System")) {
				messageLbl.setFont(Font.font(messageLbl.getFont().getFamily(), FontPosture.ITALIC, messageLbl.getFont().getSize()));
			}
			
			HBox box = new HBox();			
			box.getChildren().addAll(senderLbl,messageLbl);

			setText(null);
			setGraphic(box);
		}
	}

}
