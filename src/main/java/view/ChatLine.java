package view;

public class ChatLine {
	
	private final String senderName;
	private final String message;
	
	public ChatLine(String senderName, String message) {
		super();
		this.senderName = senderName;
		this.message = message;
	}

	public String getSenderName() {
		return senderName;
	}

	public String getMessage() {
		return message;
	}
	
	

}
